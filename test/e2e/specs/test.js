// For authoring Nightwatch tests, see
// http://nightwatchjs.org/guide#usage

module.exports = {
  'initial test': function (browser) {
    // automatically uses dev Server port from /config.index.js
    // default: http://localhost:8080
    // see nightwatch.conf.js
    const devServer = browser.globals.devServerURL;

    browser
      .url(devServer)
      .waitForElementVisible('#app', 3000)
      .assert.elementPresent('.page-entry');
  },
  'change page': function (browser) {
    const devServer = browser.globals.devServerURL;

    browser
      .url(`${devServer}/#/pg/overview`)
      .waitForElementVisible('#app', 3000)
      .assert.elementPresent('.page-entry')
      .waitForElementNotPresent('.page-entry__loading', 2000)
      .click('.left-menu ul li:nth-child(2) a')
      .waitForElementVisible('.page-entry__loading', 50)
      .assert.urlContains('#/pg/news')
      .waitForElementNotPresent('.page-entry__loading', 2000)
      .end();
  },
};
