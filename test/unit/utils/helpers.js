export function externalComponentStub(slots) {
  return {
    render(h) {
      return h(
        'div',
        { class: 'stub-slots' },
        slots.map(x => h('div', { class: `stub-slot-${x}` }, this.$slots[x])));
    },
  };
}
