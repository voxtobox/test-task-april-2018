import { shallow, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import PageEntry from '@/components/Pages/PageEntry';

const localVue = createLocalVue();
localVue.use(Vuex);

const mockGetters = {
  loading: () => false,
  pageData: () => ({
    foo: 'bar',
  }),
  componentName: () => 'fooBar',
};

const getStore = getters => new Vuex.Store({
  modules: {
    page: {
      namespaced: true,
      getters,
    },
  },
});


const store = getStore(mockGetters);

const factory = (options = {}) => (
  shallow(PageEntry, {
    localVue,
    store,
    stubs: ['fooBar'],
    ...options,
  })
);

describe('PageEntry.vue', () => {
  describe('loading block', () => {
    it('should show if getter loading is true', () => {
      const altStore = getStore({
        ...mockGetters,
        loading: () => true,
      });
      const wrapper = factory({ store: altStore });
      expect(wrapper.find('.page-entry__loading').exists()).toBe(true);
    });
    it('should hide if getter loading is false', () => {
      const wrapper = factory();
      expect(wrapper.find('.page-entry__loading').exists()).toBe(false);
    });
  });
});
