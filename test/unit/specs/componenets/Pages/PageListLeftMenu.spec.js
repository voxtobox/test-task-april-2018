import { shallow } from '@vue/test-utils';
import PageListLeftMenu from '@/components/Pages/PageListLeftMenu';
import { externalComponentStub } from '../../../utils/helpers';

const slots = ['default', 'headerTitle', 'pageName'];

const factory = (options = {}) => (
  shallow(PageListLeftMenu, {
    stubs: {
      LayoutLeftMenu: externalComponentStub(slots),
    },
    ...options,
  })
);

const propsData = {
  title: 'titleMock',
  name: 'nameMock',
  content: '<div class="content-mock">contentMock</div>',
  childrens: [{
    image: 'imageUrl1',
    url: 'url1',
    title: 'title1',
    intro: 'intro1',
  }, {
    image: 'imageUrl2',
    url: 'url2',
    title: 'title2',
    intro: 'intro2',
  }],
};

describe('PageListLeftMenu.vue', () => {
  const wrapper = factory({
    propsData: { ...propsData },
  });
  describe('passing props to LayoutLeftMenu', () => {
    it('should pass title to headerTitle slot', () => {
      expect(wrapper.find('.stub-slot-headerTitle').text()).toBe(propsData.title);
    });
    it('should pass name to pageName slot', () => {
      expect(wrapper.find('.stub-slot-pageName').text()).toBe(propsData.name);
    });
    it('should pass content to default slot as html', () => {
      const defaultSlot = wrapper.find('.stub-slot-default');
      expect(defaultSlot.find('.content-mock').html()).toBe(propsData.content);
    });
  });
  describe('rendering childrens as list', () => {
    const list = wrapper.find('.children-list');
    it('should render as unordered list', () => {
      expect(list.is('ul')).toBe(true);
    });
    it('should has the same number of item as childrens length', () => {
      expect(list.findAll('li').length).toBe(propsData.childrens.length);
    });
  });
});
