import { shallow } from '@vue/test-utils';
import PageLeftMenu from '@/components/Pages/PageLeftMenu';
import { externalComponentStub } from '../../../utils/helpers';

const slots = ['default', 'headerTitle', 'pageName'];

const factory = (options = {}) => (
  shallow(PageLeftMenu, {
    stubs: {
      LayoutLeftMenu: externalComponentStub(slots),
    },
    ...options,
  })
);

const propsData = {
  title: 'titleMock',
  name: 'nameMock',
  content: '<div class="content-mock">contentMock</div>',
  childrens: [],
};

describe('PageListLeftMenu.vue', () => {
  const wrapper = factory({
    propsData: { ...propsData },
  });
  describe('passing props to LayoutLeftMenu', () => {
    it('should pass title to headerTitle slot', () => {
      expect(wrapper.find('.stub-slot-headerTitle').text()).toBe(propsData.title);
    });
    it('should pass name to pageName slot', () => {
      expect(wrapper.find('.stub-slot-pageName').text()).toBe(propsData.name);
    });
    it('should pass content to default slot as html', () => {
      const defaultSlot = wrapper.find('.stub-slot-default');
      expect(defaultSlot.find('.content-mock').html()).toBe(propsData.content);
    });
  });
});
