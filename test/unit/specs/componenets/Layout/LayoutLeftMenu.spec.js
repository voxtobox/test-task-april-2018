import { shallow } from '@vue/test-utils';
import LayoutLeftMenu from '@/components/Layout/LayoutLeftMenu';

const factory = (options = {}) => (
  shallow(LayoutLeftMenu, {
    stubs: {
      TheHeaderTitle: {
        render(h) {
          return h(
            'div',
            this.$slots.default,
          );
        },
      },
    },
    ...options,
  })
);

const slots = {
  default: '<div class="mock-default-slot">Mock default content</div>',
  headerTitle: '<div class="mock-headerTitle-slot">Mock headerTitle content</div>',
  pageName: '<div class="mock-pageName-slot">Mock pageName content</div>',
};

describe('LayoutLeftMenu.vue', () => {
  const wrapper = factory({ slots });
  it('should render default slot', () => {
    expect(wrapper.find('.mock-default-slot').html()).toBe(slots.default);
  });
  it('should render headerTitle slot', () => {
    expect(wrapper.find('.mock-headerTitle-slot').html()).toBe(slots.headerTitle);
  });
  it('should render pageName slot', () => {
    expect(wrapper.find('.mock-pageName-slot').html()).toBe(slots.pageName);
  });
});
