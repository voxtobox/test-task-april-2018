import { shallow } from '@vue/test-utils';
import TheHeaderTitle from '@/components/Layout/TheHeaderTitle';

const factory = (options = {}) => (
  shallow(TheHeaderTitle, { ...options })
);

describe('TheHeaderTitle.vue', () => {
  it('should render default slot', () => {
    const wrapper = factory({
      slots: { default: '<div class="mock-default-slot">lorem ipsum</div>' },
    });
    expect(wrapper.find('div.mock-default-slot').text()).toBe('lorem ipsum');
  });
});
