import mutations, * as mutationsTypes from '@/store/page/mutations';

describe('page module mutations', () => {
  describe(`mutation ${mutationsTypes.SET_PAGE_DATA}`, () => {
    it('should set passed data as pageData', () => {
      const state = { pageData: {} };
      const data = { foo: 'bar' };
      mutations[mutationsTypes.SET_PAGE_DATA](state, data);
      expect(state.pageData).toBe(data);
    });
  });
  describe(`mutation ${mutationsTypes.SET_LOADING}`, () => {
    const state = { loading: false };
    it('should set passed value as loading', () => {
      mutations[mutationsTypes.SET_LOADING](state, true);
      expect(state.loading).toBe(true);
    });
    it('should set loading true when no passed value', () => {
      state.loading = false;
      mutations[mutationsTypes.SET_LOADING](state);
      expect(state.loading).toBe(true);
    });
  });
});
