export function resolveDependencies(store, to, from, next) {
  if (to.name === 'page') {
    store.dispatch('page/getPageData', to.params.pageUrl);
  }
  next();
}
