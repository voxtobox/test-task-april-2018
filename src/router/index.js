import Vue from 'vue';
import Router from 'vue-router';
import PageEntry from '@/components/Pages/PageEntry';
import store from '@/store';
import { resolveDependencies } from './dependency-manager';

Vue.use(Router);

const router = new Router({
  routes: [
    {
      path: '/',
      redirect: '/pg/overview',
    },
    {
      path: '/pg/:pageUrl',
      name: 'page',
      component: PageEntry,
    },
  ],
  scrollBehavior() {
    return { x: 0, y: 0 };
  },
});

router.beforeEach((to, from, next) => resolveDependencies(store, to, from, next));

export default router;
