export const SET_PAGE_DATA = 'SET_PAGE_DATA';
export const SET_LOADING = 'SET_LOADING';

export default {
  [SET_PAGE_DATA](state, pageData) {
    Object.assign(state, { pageData });
    return state.pageData;
  },
  [SET_LOADING](state, loading = true) {
    Object.assign(state, { loading });
    return state.loading;
  },
};
