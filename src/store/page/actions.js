import * as api from '@/services/api';
import { SET_LOADING, SET_PAGE_DATA } from './mutations';

export default {
  getPageData({ commit }, pageUrl) {
    commit(SET_LOADING, true);
    api.getPageData(pageUrl)
      .then(({ data }) => {
        commit(SET_PAGE_DATA, data);
        commit(SET_LOADING, false);
      });
  },
};
