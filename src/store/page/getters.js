import templateComponentMap from '@/config/templateComponentMap';

export default {
  loading: state => state.loading,
  pageData: state => state.pageData,
  componentName: state => templateComponentMap[state.pageData.template],
};
