import * as mocks from '@/mocks';

export const getPageData = pageName => new Promise((resolve) => {
  setTimeout(() => {
    resolve({
      data: mocks[pageName],
    });
  }, 500);
});
