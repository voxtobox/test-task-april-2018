export const overview = {
  id: 1,
  url: 'overview',
  name: 'OVERVIEW',
  title: 'GROWTHFOUNTAIN FEATURES',
  template: 'page_leftmenu',
  content: '<img class="m-b-3 fade-in" src="https://growthfountain.com/staticdata/img/pages/overview.d482bb57f69a24679dd36d6b01329677.jpg" alt=""><p class="m-b-2 text-justify fade-in">We banded together with a simple mission – to connect small businesses to the tools they need to succeed – including financial, human and social capital. Our goal is to level the playing field and make things easy for entrepreneurs to focus on what matters – their product or service! Please feel free to take advantage of all of the tools, tutorials and calculators we have available on our platform, including:</p><div class="list-block"><div class="row m-b-2 text-xs-center text-sm-left text-md-left text-lg-left text-xl-left fade-in"><div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-12"><div class="icon-in-circle"><i class="fa fa-diamond"></i></div></div><div class="col-xl-11 col-lg-11 col-md-11 col-sm-11 col-xs-12 text-block"><a class="black-link" href="/raise-capital" target="_blank"><h4 class="text-uppercase">Raise capital </h4></a><p>Access our investor network to raise financial capital for your company</p></div></div><div class="row m-b-2 text-xs-center text-sm-left text-md-left text-lg-left text-xl-left fade-in"><div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-12"><div class="icon-in-circle"><i class="fa fa-book"></i></div></div><div class="col-xl-11 col-lg-11 col-md-11 col-sm-11 col-xs-12 text-block"><a class="black-link" href="/pg/education" target="_blank"><h4 class="text-uppercase">Education Material </h4></a><p>How does investing in Regulation Crowdfunding offerings actually work?</p></div></div><div class="row m-b-2 text-xs-center text-sm-left text-md-left text-lg-left text-xl-left fade-in"><div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-12"><div class="icon-in-circle"><i class="fa fa-check-square-o"></i></div></div><div class="col-xl-11 col-lg-11 col-md-11 col-sm-11 col-xs-12 text-block"><a class="black-link" href="/pg/success-guide" target="_blank"><h4 class="text-uppercase">Success Guide</h4></a><p>This tool can help your company prepare your fundraising campaign</p></div></div><div class="row m-b-2 text-xs-center text-sm-left text-md-left text-lg-left text-xl-left fade-in"><div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-12"><div class="icon-block"><div class="icon-in-circle"><i class="fa fa-list-ol"></i></div></div></div><div class="col-xl-11 col-lg-11 col-md-11 col-sm-11 col-xs-12 text-block"><a class="d-inline-block black-link" href="/pg/investor-tutorial" target="_blank"><h4 class="text-uppercase">Investor</h4></a><h4 class="text-uppercase d-inline-block">&nbsp;and&nbsp;</h4><a class="d-inline-block black-link" href="/pg/business-tutorial" target="_blank"><h4 class="text-uppercase">Business Tutorials</h4></a><p>Delve deeper into the rules and obligations of Regulation Crowdfunding</p></div></div><div class="row m-b-2 text-xs-center text-sm-left text-md-left text-lg-left text-xl-left fade-in"><div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-12"><div class="icon-block"><div class="icon-in-circle"><i class="fa fa-line-chart"></i></div></div></div><div class="col-xl-11 col-lg-11 col-md-11 col-sm-11 col-xs-12 text-block"><a class="black-link" href="/calculator/capital-raise" target="_blank"><h4 class="text-uppercase">Capital Raise Calculator</h4></a><p>This tool helps you understand how much money you might need to raise to accomplish your goals</p></div></div><div class="row m-b-2 text-xs-center text-sm-left text-md-left text-lg-left text-xl-left fade-in"><div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-12"><div class="icon-block"><div class="icon-in-circle"><i class="fa fa-briefcase"></i></div></div></div><div class="col-xl-11 col-lg-11 col-md-11 col-sm-11 col-xs-12 text-block"><a class="black-link" href="/calculator/business-valuation" target="_blank"><h4 class="text-uppercase">What’s my Business Worth?</h4></a><p>This tool helps you benchmark valuations for similar companies</p></div></div><div class="row text-xs-center text-sm-left text-md-left text-lg-left text-xl-left fade-in"><div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-12"><div class="icon-block"><div class="icon-in-circle"><i class="fa fa-question"></i></div></div></div><div class="col-xl-11 col-lg-11 col-md-11 col-sm-11 col-xs-12 text-block"><a class="black-link" href="/pg/faq" target="_blank"><h4 class="text-uppercase">FAQ</h4></a><p>Scroll through our Frequently Asked Questions to gain additional insight into how it all works</p></div></div><div class="row"><div class="col-xl-12 text-xs-center"><h3 class="font-weight-light">Together, we can help support entrepreneurs and grow businesses<br>that otherwise might not have had this opportunity.</h3></div></div></div>',
  childrens: [],
};

export const news = {
  id: 23,
  url: '/news',
  name: 'MEDIA COVERAGE OF GROWTHFOUNTAIN',
  title: 'GROWTHFOUNTAIN PRESS',
  content: '<h6 class="font-weight-light text-xs-center m-b-3 m-t-0">GrowthFountain helps businesses raise capital online with its equity crowdfunding portal. For press features or speaking opportunities for GrowthFountain or CEO Kenneth Staut, please contact info@growthfountain.com</h6>',
  template: 'list_leftmenu',
  childrens: [
    {
      id: 24,
      url: '/news/24',
      intro: '<p>The crowdfunding phenomenon has been fascinating to watch, but it never seemed relevant to any of my businesses, until now. There\'s a new form of crowdfunding being led by companies such as GrowthFountain...</p>',
      title: 'Why Crowdfunding Suddenly Looks Good to This Serial Entrepreneur',
      image: 'https://growthfountain.com/staticdata/img/pages/inc.7c91b3087d3d6c5958d80034b8fe0848.png',
    },
    {
      id: 25,
      url: '/news/25',
      intro: '<p>There’s no shortage of equity crowdfunding sites out there, but GrowthFountain thinks it has the special sauce to cut through the mayhem by thinking close to home. Focusing on investors local (or at least regional)...</p>',
      title: 'New equity crowdfunding platform GrowthFountain launched today',
      image: 'https://growthfountain.com/staticdata/img/pages/TC.8a8dd61c6877b2a5ceae3b912fc1bb53.jpg',
    },
  ],
};
